#!/usr/bin/python3
import subprocess
from random import randint
import argparse

parser = argparse.ArgumentParser(description='Generate PWs from 40k most common english words')
parser.add_argument('n_words', metavar='N', type=int, nargs='?', help='number of words to draw')
args = parser.parse_args()

try:
    n_words = int(args.n_words)
    still_trying = False
except:
    still_trying = True

while still_trying:
    try:
        n_words = int(input("Please tell us the number of words you want to draw: "))
        still_trying = False
    except TypeError:
        print("Invalid input, try again.")

if n_words < 4:
    print("Beware, this password will be weak.")

lines_to_read = list()
for i in range(n_words):
    lines_to_read.append(randint(0, 49999))
lines_to_read.sort()

read_words = list()
with open("50k.txt") as fp:
    for i, line in enumerate(fp):
        if i == lines_to_read[0]:
            read_words.append(line[:-1])
            if len(lines_to_read) == 1:
                break
            else:
                del lines_to_read[0]
print(" ".join(read_words))
